import { Component, Input } from '@angular/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCarouselModule } from 'ng-zorro-antd/carousel';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzTypographyModule } from 'ng-zorro-antd/typography';

@Component({
  selector: 'app-carousel-img',
  standalone: true,
  templateUrl: './carousel-img.component.html',
  styleUrl: './carousel-img.component.less',
  imports: [
    NzTypographyModule,
    NzIconModule,
    NzCarouselModule,
    NzImageModule,
    NzButtonModule,
    NzGridModule,
  ],
})
export class CarouselImgComponent {
  @Input({ required: true }) imageUrls!: string[];
  @Input() noPreview?: boolean;
}
