import { NgModule } from '@angular/core';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { ModalStatusComponent } from './modal-status.component';
import { ModalStatusService } from './modal-status.service';

@NgModule({
  imports: [NzModalModule, ModalStatusComponent],
  exports: [ModalStatusComponent],
  providers: [ModalStatusService],
})
export class ModalStatusModule {}
