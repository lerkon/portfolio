import { NgTemplateOutlet } from '@angular/common';
import { Component, OnDestroy, inject } from '@angular/core';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NZ_MODAL_DATA, NzModalRef } from 'ng-zorro-antd/modal';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { Subscription, forkJoin, of } from 'rxjs';
import {
  ModalStatusComponentProps,
  ModalStatusData,
} from './modal-status.types';

@Component({
  selector: 'app-modal-status',
  standalone: true,
  templateUrl: './modal-status.component.html',
  styleUrl: './modal-status.component.less',
  imports: [
    NgTemplateOutlet,
    TranslateModule,
    NzGridModule,
    NzTypographyModule,
    NzIconModule,
    NzButtonModule,
  ],
})
export class ModalStatusComponent implements OnDestroy {
  errorMsg?: string;
  currentView?: 'confirm' | 'processing' | 'error';

  readonly confirmMsg: ModalStatusComponentProps['confirmMsg'];
  readonly processingMsg: NonNullable<
    ModalStatusComponentProps['processingMsg']
  >;
  readonly errorTitle: NonNullable<ModalStatusComponentProps['errorTitle']>;

  private processSubscription?: Subscription;
  private readonly modal: NzModalRef<ModalStatusComponent, ModalStatusData>;
  private readonly modalData: ModalStatusComponentProps;
  private timeoutId?: ReturnType<typeof setTimeout>;

  constructor(
    private translateService: TranslateService,
    private nzMessageService: NzMessageService
  ) {
    this.modal = inject(NzModalRef);
    this.modalData = inject(NZ_MODAL_DATA);

    this.confirmMsg = this.modalData.confirmMsg;
    this.processingMsg = this.modalData.processingMsg || 'Processing';
    this.errorTitle = this.modalData.errorTitle || 'An error has occurred';

    if (this.confirmMsg) {
      this.currentView = 'confirm';
    } else {
      this.onConfirm();
    }
  }

  ngOnDestroy(): void {
    this.processSubscription?.unsubscribe();
    clearTimeout(this.timeoutId);
  }

  onCancel(): void {
    this.modal.close();
  }

  async onConfirm(): Promise<void> {
    const nzWidth = this.modal.containerInstance.config.nzWidth;

    this.currentView = 'processing';

    this.modal.updateConfig({
      nzClosable: false,
      nzKeyboard: false,
      nzMaskClosable: false,
      nzWidth: 'initial',
    });

    const successToastMsg$ = this.modalData.successToastMsg
      ? this.translateService.get(this.modalData.successToastMsg)
      : of(undefined);

    const delayPromise = new Promise(
      (resolve) => (this.timeoutId = setTimeout(resolve, 500))
    );

    this.processSubscription = forkJoin([
      this.modalData.process$,
      successToastMsg$,
    ]).subscribe({
      next: async ([res, successToastMsg]) => {
        await delayPromise; // Prevent flickering if the process is too fast.
        this.modal.close({ cancel: false, data: res });
        if (successToastMsg) this.nzMessageService.success(successToastMsg);
      },
      error: (error) => {
        this.currentView = 'error';

        if (error instanceof Error) {
          this.errorMsg = error.message;
        } else if (typeof error === 'object') {
          this.errorMsg = JSON.stringify(error, null, 2);
        } else {
          this.errorMsg = error?.toString();
        }

        this.modal.updateConfig({
          nzMaskClosable: true,
          nzClosable: true,
          nzWidth,
        });
      },
    });
  }
}
