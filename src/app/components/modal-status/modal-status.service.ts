import { Injectable } from '@angular/core';
import { ModalOptions, NzModalService } from 'ng-zorro-antd/modal';
import { Observable, map } from 'rxjs';
import { ModalStatusComponent } from './modal-status.component';
import {
  ModalStatusComponentProps,
  ModalStatusData,
} from './modal-status.types';

@Injectable({ providedIn: 'root' })
export class ModalStatusService {
  constructor(private nzModalService: NzModalService) {}

  open<T = unknown>(
    componentProps: ModalStatusComponentProps,
    options?: Omit<ModalOptions, 'nzContent' | 'nzData'>
  ): Observable<ModalStatusData<T>> {
    return this.nzModalService
      .create<
        ModalStatusComponent,
        ModalStatusComponentProps,
        ModalStatusData<T>
      >({
        nzFooter: null,
        nzCentered: true,
        ...options,
        nzContent: ModalStatusComponent,
        nzData: componentProps,
      })
      .afterClose.pipe(map((res) => res ?? { cancel: true }));
  }
}
