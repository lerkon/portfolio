import { Observable } from 'rxjs';

export interface ModalStatusComponentProps {
  process$: Observable<unknown>;
  confirmMsg?: string;
  processingMsg?: string;
  successToastMsg?: string;
  errorTitle?: string;
}

export interface ModalStatusData<T = unknown> {
  cancel: boolean;
  data?: T;
}
