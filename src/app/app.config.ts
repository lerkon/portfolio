import { PlatformLocation, registerLocaleData } from '@angular/common';
import { HttpClient, provideHttpClient } from '@angular/common/http';
import en from '@angular/common/locales/en';
import {
  APP_INITIALIZER,
  ApplicationConfig,
  importProvidersFrom,
} from '@angular/core';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { provideRouter } from '@angular/router';
import { FaConfig, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faCircleHalfStroke } from '@fortawesome/free-solid-svg-icons';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { en_US, provideNzI18n } from 'ng-zorro-antd/i18n';
import { routes } from './app.routes';

registerLocaleData(en);

export const appConfig: ApplicationConfig = {
  providers: [
    provideAnimationsAsync(),
    provideHttpClient(),
    provideRouter(routes),
    provideNzI18n(en_US),
    importProvidersFrom(
      TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          deps: [HttpClient, PlatformLocation],
          useFactory: (
            http: HttpClient,
            platformLocation: PlatformLocation
          ): TranslateHttpLoader =>
            new TranslateHttpLoader(
              http,
              platformLocation.getBaseHrefFromDOM() + 'assets/i18n/'
            ),
        },
      })
    ),
    {
      provide: APP_INITIALIZER,
      multi: true,
      deps: [FaIconLibrary, FaConfig],
      useFactory: (iconLibrary: FaIconLibrary, config: FaConfig) => {
        return () => {
          // far (regular); fas (solid); fal (light); fab (brands)
          config.defaultPrefix = 'far';
          iconLibrary.addIcons(faCircleHalfStroke);
          return iconLibrary;
        };
      },
    },
  ],
};
