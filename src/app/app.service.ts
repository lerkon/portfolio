import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map, of, tap } from 'rxjs';

export enum ThemeType {
  dark = 'dark',
  default = 'default',
}

@Injectable({ providedIn: 'root' })
export class AppService {
  private currentTheme: ThemeType;

  private readonly styleEl: HTMLStyleElement;
  private readonly styleCache: Partial<Record<ThemeType, string>>;

  constructor(private httpClient: HttpClient) {
    this.styleCache = {};
    this.styleEl = document.createElement('style');

    this.currentTheme =
      (window.localStorage.getItem('theme') as ThemeType) || ThemeType.default;
  }

  intiTheme(): Observable<ThemeType> {
    document.body.hidden = true;
    document.head.appendChild(this.styleEl);
    document.documentElement.classList.add(this.currentTheme);

    return this.setTheme(this.currentTheme).pipe(
      tap(() => (document.body.hidden = false))
    );
  }

  toggleTheme(): Observable<ThemeType> {
    return this.setTheme(this.getOppositeTheme(this.currentTheme));
  }

  private setTheme(theme: ThemeType): Observable<ThemeType> {
    return (
      Object.hasOwn(this.styleCache, theme)
        ? of(this.styleCache[theme]!)
        : this.httpClient.get(`${theme}.css`, { responseType: 'text' })
    ).pipe(
      map((cssStr) => {
        document.documentElement.classList.replace(
          this.getOppositeTheme(theme),
          theme
        );

        this.currentTheme = theme;
        this.styleEl.innerHTML = cssStr;
        this.styleCache[theme] ||= cssStr;
        window.localStorage.setItem('theme', theme);

        return theme;
      })
    );
  }

  private getOppositeTheme(theme: ThemeType): ThemeType {
    return theme === ThemeType.dark ? ThemeType.default : ThemeType.dark;
  }
}
