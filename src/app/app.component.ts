import {
  animate,
  query,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { NgTemplateOutlet, UpperCasePipe } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { RouterModule, RouterOutlet } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { Subscription } from 'rxjs';
import { AppService, ThemeType } from './app.service';
import {
  ModalStatusModule,
  ModalStatusService,
} from './components/modal-status';

type MenuItem = { link: string; label: string };

const routeTransitionAnimation = trigger('routeName', [
  transition('* <=> *', [
    query(
      ':enter',
      [
        style({
          transform: 'translateY(100%)',
          opacity: 0,
          position: 'fixed',
          width: 'var(--app-width-body)',
        }),
        animate(
          '0.5s ease-in-out',
          style({ transform: 'translateY(0%)', opacity: 1 })
        ),
      ],
      { optional: true }
    ),
    query(':leave', style({ display: 'none' }), { optional: true }),
  ]),
]);

@Component({
  selector: 'app-root',
  standalone: true,
  styleUrl: './app.component.less',
  templateUrl: './app.component.html',
  animations: [routeTransitionAnimation],
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    RouterModule,
    NgTemplateOutlet,
    UpperCasePipe,
    TranslateModule,
    FontAwesomeModule,
    NzLayoutModule,
    NzMenuModule,
    NzGridModule,
    NzDropDownModule,
    NzIconModule,
    NzButtonModule,
    NzDrawerModule,
    ModalStatusModule,
  ],
})
export class AppComponent implements OnInit, OnDestroy {
  lang: string;
  theme?: ThemeType;

  readonly menuItems: MenuItem[];
  private subscriptions: Subscription[] = [];

  constructor(
    private translateService: TranslateService,
    private appService: AppService,
    private modalStatusService: ModalStatusService
  ) {
    this.translateService.setDefaultLang('en');
    this.lang = window.localStorage.getItem('language') || 'en';
    this.translateService.use(this.lang);

    this.menuItems = [
      { link: '/projects', label: 'Projects' },
      { link: '/contact', label: 'Contact' },
    ];
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((e) => e.unsubscribe());
  }

  ngOnInit(): void {
    this.subscriptions.push(
      this.appService.intiTheme().subscribe((theme) => (this.theme = theme)),
      this.translateService
        .stream('_tabTitle')
        .subscribe((title) => (document.title = title))
    );
  }

  onChangeLanguage(lang: string): void {
    if (this.lang === lang) return;
    this.lang = lang;
    this.translateService.use(lang);
    window.localStorage.setItem('language', lang);
  }

  getRouteName(outlet: RouterOutlet): string {
    return outlet.isActivated
      ? outlet.activatedRoute.snapshot.url.toString()
      : '';
  }

  toggleTheme(): void {
    this.modalStatusService
      .open<ThemeType>({ process$: this.appService.toggleTheme() })
      .subscribe((res) => {
        if (res.cancel) return;
        this.theme = res.data;
      });
  }
}
