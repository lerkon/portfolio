import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy } from '@angular/core';
import {
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { Observable, Subscription } from 'rxjs';
import { environment } from '../../../environments/environment';
import {
  ModalStatusModule,
  ModalStatusService,
} from '../../components/modal-status';

@Component({
  selector: 'app-contact',
  standalone: true,
  templateUrl: './contact.component.html',
  styleUrl: './contact.component.less',
  imports: [
    ReactiveFormsModule,
    NzFormModule,
    NzInputModule,
    NzButtonModule,
    NzIconModule,
    TranslateModule,
    NzTypographyModule,
    NzGridModule,
    ModalStatusModule,
  ],
})
export class ContactComponent implements OnDestroy {
  modalOpenSubscription?: Subscription;
  readonly formGroup: FormGroup;

  constructor(
    private httpClient: HttpClient,
    private modalStatusService: ModalStatusService
  ) {
    this.formGroup = new FormGroup({
      name: new FormControl(''),
      email: new FormControl('', [Validators.required, Validators.email]),
      message: new FormControl('', [
        Validators.required,
        Validators.pattern(/\S/),
      ]),
    });
  }

  ngOnDestroy(): void {
    this.modalOpenSubscription?.unsubscribe();
  }

  onSubmit(): void {
    if (this.formGroup.invalid) {
      Object.values(this.formGroup.controls).forEach((control) => {
        if (control.valid) return;
        control.markAsDirty();
        control.updateValueAndValidity({ onlySelf: true });
      });

      return;
    }

    this.modalOpenSubscription = this.modalStatusService
      .open({
        process$: this.sendEmail(),
        confirmMsg: '_confirmMsgSend',
        successToastMsg: '_msgSentSuccess',
      })
      .subscribe((res) => {
        if (res.cancel) return;
        this.formGroup.reset();
      });
  }

  private sendEmail(): Observable<unknown> {
    const formData = new FormData();

    for (let k in this.formGroup.controls) {
      formData.append(k, this.formGroup.controls[k].value.trim());
    }

    return this.httpClient.post(environment.contactUrl, this.formGroup.value);
  }
}
