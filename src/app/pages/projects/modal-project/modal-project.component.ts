import { Component, inject } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { NZ_MODAL_DATA } from 'ng-zorro-antd/modal';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { Project } from '../projects.component.data';

export type ModalProjectComponentProps = {
  project: Project;
};

@Component({
  selector: 'app-modal-project',
  standalone: true,
  templateUrl: './modal-project.component.html',
  styleUrl: './modal-project.component.less',
  imports: [TranslateModule, NzTypographyModule],
})
export class ModalProjectComponent {
  readonly project: ModalProjectComponentProps['project'];
  private readonly modalData: ModalProjectComponentProps;

  constructor() {
    this.modalData = inject(NZ_MODAL_DATA);
    this.project = this.modalData.project;
  }
}
