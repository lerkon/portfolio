import { NgModule } from '@angular/core';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { ModalProjectComponent } from './modal-project.component';
import { ModalProjectService } from './modal-project.service';

@NgModule({
  imports: [NzModalModule, ModalProjectComponent],
  exports: [ModalProjectComponent],
  providers: [ModalProjectService],
})
export class ModalProjectModule {}
