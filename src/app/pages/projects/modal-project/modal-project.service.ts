import { Injectable } from '@angular/core';
import { ModalOptions, NzModalService } from 'ng-zorro-antd/modal';
import { Observable } from 'rxjs';
import {
  ModalProjectComponent,
  ModalProjectComponentProps,
} from './modal-project.component';

@Injectable({ providedIn: 'root' })
export class ModalProjectService {
  constructor(private nzModalService: NzModalService) {}

  open(
    componentProps: ModalProjectComponentProps,
    options?: Omit<ModalOptions, 'nzContent' | 'nzData'>
  ): Observable<void> {
    return this.nzModalService.create<
      ModalProjectComponent,
      ModalProjectComponentProps
    >({
      nzTitle: componentProps.project.title,
      nzFooter: null,
      nzCentered: true,
      ...options,
      nzContent: ModalProjectComponent,
      nzData: componentProps,
    }).afterClose;
  }
}
