import { AsyncPipe } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzTabsModule } from 'ng-zorro-antd/tabs';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { Observable, forkJoin, map } from 'rxjs';
import { environment } from '../../../environments/environment';
import { CarouselImgComponent } from '../../components/carousel-img';
import { ModalProjectModule, ModalProjectService } from './modal-project';
import {
  Project,
  projectCategories,
  projects,
} from './projects.component.data';

@Component({
  selector: 'app-projects',
  standalone: true,
  templateUrl: './projects.component.html',
  styleUrl: './projects.component.less',
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    AsyncPipe,
    TranslateModule,
    NzTabsModule,
    NzTypographyModule,
    NzCardModule,
    NzIconModule,
    NzGridModule,
    NzButtonModule,
    ModalProjectModule,
    CarouselImgComponent,
  ],
})
export class ProjectsComponent {
  readonly projectCategories = projectCategories;
  readonly projects = projects;
  readonly environment = environment;

  constructor(
    private translateService: TranslateService,
    private modalProjectService: ModalProjectService
  ) {}

  getCardSubTitle(project: Project): Observable<string> {
    return forkJoin([
      this.translateService.get('Private project'),
      this.translateService.get('Internship'),
    ]).pipe(
      map(([labelPrivateProject, labelInternship]) => {
        if (!project.company) return labelPrivateProject;

        let subTitle = project.company;

        if (project.categoryIds.includes('internship')) {
          subTitle += ` ${labelInternship}`;
        }

        return subTitle;
      }),
      map((subTitle) => subTitle.toUpperCase())
    );
  }

  onOpenLearnMore(project: Project): void {
    this.modalProjectService.open({ project });
  }
}
