export const environment = {
  production: false,
  contactUrl: 'https://formspree.io/f/mrgnyzgy',
  repoUrl: 'https://gitlab.com/users/lerkon/projects',
  linkedinUrl: 'https://www.linkedin.com/in/laptmarek',
};
